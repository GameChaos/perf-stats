
#if defined _colors_included
 #endinput
#endif
#define _colors_included

#define C_DEFAULT    "\x01"
#define C_DARKRED    "\x02"
#define C_GREEN      "\x04"
#define C_LIGHTGREEN "\x03"
#define C_ORANGE     "\x03"
#define C_BLUE       "\x03"
#define C_OLIVE      "\x05"
#define C_LIME       "\x06"
#define C_RED        "\x07"
#define C_PURPLE     "\x03"
#define C_GREY       "\x08"
#define C_YELLOW     "\x09"
#define C_LIGHTBLUE  "\x0A"
#define C_STEELBLUE  "\x0B"
#define C_DARKBLUE   "\x0C"
#define C_PINK       "\x0E"
#define C_LIGHTRED   "\x0F"