# Perf Stats

A plugin that records bhop statistics of players.

**DISCLAIMER:**

This is probably pretty slow and it might lag your server, so I suggest not putting it on public servers where a lot of people play.

# Commands

All commands support looking up SteamID's. The only SteamID format currently supported is "STEAM_X:X:XXXXX".

Example: `sm_perfstats "STEAM_1:0:102468802"`

* **sm_perfstats** - Prints bhop stats to the chat and console.  

Example console output:

```
[GC] Frames on ground stats:
Bhop count: 1646
 tick 1:     695 | 42.22%
 tick 2:     696 | 42.28%
 tick 3:     225 | 13.66%
 tick 4:      22 |  1.33%
 tick 5:       6 |  0.36%
 tick 6:       1 |  0.06%
 tick 7:       1 |  0.06%
 tick 8:       0 |  0.00%
```

* **sm_perfstreaks** - Prints perfect bhop streak counts to the console.

Example:
```
[GC] Perf streaks:  
Perf count: 465  
Perfs  1:     319 | 68.60%  
Perfs  2:      92 | 19.78%  
Perfs  3:      34 |  7.31%  
Perfs  4:      14 |  3.01%  
Perfs  5:       3 |  0.64%  
Perfs  6:       3 |  0.64%  
Perfs  7:       0 |  0.00%  
Perfs  8:       0 |  0.00%  
Perfs  9:       0 |  0.00%  
Perfs 10:       0 |  0.00%  
Perfs 11:       0 |  0.00%  
Perfs 12:       0 |  0.00%  
Perfs 13:       0 |  0.00%  
Perfs 14:       0 |  0.00%  
Perfs 15:       0 |  0.00%  
Perfs 16:       0 |  0.00%  
Perfs 17:       0 |  0.00%  
Perfs 18:       0 |  0.00%  
Perfs 19:       0 |  0.00%  
Perfs 20:       0 |  0.00%  
Perfs 21:       0 |  0.00%  
Perfs 22:       0 |  0.00%  
Perfs 23:       0 |  0.00%  
Perfs 24:       0 |  0.00%  
```
* **sm_perfstreaks** - Prints data about when you scrolled in relation to the first ground tick.

Example:
```
[GC] Scroll stats:  
Scroll count: 11700  
     -15:     102 |  0.87%  
     -14:     146 |  1.24%  
     -13:     154 |  1.31%  
     -12:     217 |  1.85%  
     -11:     245 |  2.09%  
     -10:     345 |  2.94%  
      -9:     376 |  3.21%  
      -8:     435 |  3.71%  
      -7:     512 |  4.37%  
      -6:     526 |  4.49%  
      -5:     597 |  5.10%  
      -4:     621 |  5.30%  
      -3:     637 |  5.44%  
      -2:     638 |  5.45%  
      -1:     653 |  5.58%  
GROUND 0:     634 |  5.41%  
       1:     658 |  5.62%  
       2:     590 |  5.04%  
       3:     592 |  5.05%  
       4:     592 |  5.05%  
       5:     496 |  4.23%  
       6:     514 |  4.39%  
       7:     417 |  3.56%  
       8:     321 |  2.74%  
       9:     251 |  2.14%  
      10:     170 |  1.45%  
      11:     115 |  0.98%  
      12:      57 |  0.48%  
      13:      53 |  0.45%  
      14:      25 |  0.21%  
      15:      11 |  0.09%  
```

# Changelog:

- **2.1.0 09/10/2020**

+ Added functionality to all stats commands for looking at other people's perf stats.
* Probably fixed perfstats not saving consistently.
* Made chat formatting better.

