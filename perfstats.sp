#include <sourcemod>
#include <sdktools>
#include <gamechaos>
#include <colors>

#pragma newdecls required
#pragma semicolon 1

#define PERFSTATS_DATA_PATH "data/perfdata.perfdata" // Path_SM
#define CHAT_PREFIX			C_DEFAULT..."["...C_LIME..."GC"...C_DEFAULT..."]"
#define PREFIX              "[GC]"

#define MAX_FRAMES_GROUND   8
#define MAX_PERF_STREAKS    24
#define MAX_SCROLL_TICKS    31 // max is 32
#define SCROLL_GROUND_TICK  ((MAX_SCROLL_TICKS + 1) / 2)

#define DEBUG

#if defined DEBUG
	#define ASSERT(%1) if (!(%1))SetFailState("Assertion failed: \""...#%1..."\"")
#else
	#define ASSERT(%1)%2;
#endif

#if defined DEBUG
	#define DEBUGPRINT(%1) PrintToChatAll(%1)
#else
	#define DEBUGPRINT(%1)%2;
#endif

enum struct Perfstats
{
	int steamID;
	int FOGStats[MAX_FRAMES_GROUND];
	int perfStreaks[MAX_PERF_STREAKS];
	int scrollStats[MAX_SCROLL_TICKS];
}

enum struct Movevars
{
	int framesOnGround;
	int currentPerfStreak;
	int currentScrolls;
	int lastButtons;
	int landingTick;
	bool hasJumped;
}

ArrayList g_alPerfstatsFile;
bool g_bLateLoad;
char g_szPerfstatsPath[PLATFORM_MAX_PATH];
int g_iClientPerfstatsIndex[MAXPLAYERS + 1]; // index into g_alPerfstatsFile
Movevars g_movevars[MAXPLAYERS + 1];

public Plugin myinfo =
{
	name = "perfstats",
	description = "Tracks perfect jump statistics over a long period of time",
	author = "GameChaos",
	version = "2.1.0"
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLateLoad = late;
}

public void OnPluginStart()
{
	if (g_bLateLoad)
	{
		for (int client = 1; client <= MaxClients; client++)
		{
			if (IsClientAuthorized(client))
			{
				g_iClientPerfstatsIndex[client] = -1;
				OnClientPostAdminCheck(client);
			}
		}
	}
	
	RegConsoleCmd("sm_perfstats", Command_SmPerfstats);
	RegConsoleCmd("sm_perfstreaks", Command_SmPerfstreaks);
	RegConsoleCmd("sm_scrollstats", Command_SmScrollstats);
}

public void OnPluginEnd()
{
	SavePerfstatsFile(g_szPerfstatsPath);
}

public void OnMapStart()
{
	if (g_alPerfstatsFile == INVALID_HANDLE)
	{
		g_alPerfstatsFile = new ArrayList(sizeof Perfstats);
	}
	else
	{
		g_alPerfstatsFile.Clear();
	}
	
	BuildPath(Path_SM, g_szPerfstatsPath, sizeof g_szPerfstatsPath, PERFSTATS_DATA_PATH);
	OpenPerfstatsFile(g_szPerfstatsPath);
	
}

public void OnMapEnd()
{
	SavePerfstatsFile(g_szPerfstatsPath);
}

public void OnClientPostAdminCheck(int client)
{
	if (IsFakeClient(client))
	{
		return;
	}
	
	if (g_alPerfstatsFile == INVALID_HANDLE)
	{
		g_alPerfstatsFile = new ArrayList(sizeof Perfstats);
		OpenPerfstatsFile(g_szPerfstatsPath);
	}
	
	// reset things
	Movevars tempMovevars;
	g_movevars[client] = tempMovevars;
	
	g_iClientPerfstatsIndex[client] = -1;
	CreatePerfstatsIndex(client);
}

void CreatePerfstatsIndex(int client)
{
	int steamID = GetSteamAccountID(client);
	
	for (int entry; entry < g_alPerfstatsFile.Length; entry++)
	{
		int storedSteamID = g_alPerfstatsFile.Get(entry, 0);
		
		if (storedSteamID == steamID)
		{
			g_iClientPerfstatsIndex[client] = entry;
			return;
		}
	}
	
	// create an entry if it doesn't exist
	if (g_iClientPerfstatsIndex[client] == -1)
	{
		Perfstats perfdata;
		perfdata.steamID = steamID;
		
		g_iClientPerfstatsIndex[client] = g_alPerfstatsFile.PushArray(perfdata);
	}
}

public Action Command_SmPerfstats(int client, int args)
{
	if (!IsValidClientExt(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iClientPerfstatsIndex[client] == -1)
	{
		CreatePerfstatsIndex(client);
	}
	
	// get custom steam id
	int customSteamID = 0;
	char szSteamID[32];
	if (args > 0)
	{
		GetCmdArg(1, szSteamID, sizeof szSteamID);
		
		char error[192];
		error = GetSteamIDFromString(szSteamID, sizeof szSteamID, customSteamID);
		if (error[0] != '\0')
		{
			PrintToChat(client, error);
			return Plugin_Handled;
		}
	}
	
	Perfstats perfdata;
	if (customSteamID == 0)
	{
		g_alPerfstatsFile.GetArray(g_iClientPerfstatsIndex[client], perfdata);
	}
	else
	{
		int index = GetPerfstatsIndexFromSteamID(customSteamID);
		if (index >= 0)
		{
			g_alPerfstatsFile.GetArray(index, perfdata);
		}
		else
		{
			PrintToChat(client, "%s SteamID \"%s\" was not found.", CHAT_PREFIX, szSteamID);
			return Plugin_Handled;
		}
	}
	
	char chatOutput[512];
	char consoleOutput[512];
	
	int jumpCount;
	for (int i; i < sizeof(perfdata.FOGStats); i++)
	{
		jumpCount += perfdata.FOGStats[i];
	}
	
	Format(chatOutput, sizeof chatOutput, "%s %i Bhops | ", CHAT_PREFIX, jumpCount);
	Format(consoleOutput, sizeof consoleOutput, "%s Frames on ground stats:\nBhop count: %i\n", PREFIX, jumpCount);
	
	for (int i; i < sizeof(perfdata.FOGStats); i++)
	{
		Format(chatOutput, sizeof chatOutput,
			"%s "...C_GREY..."tick "...C_DEFAULT..."%i: "...C_LIME..."%.2f%%%% "...C_DEFAULT..."|",
			chatOutput,
			i + 1,
			CalcIntPercentage(perfdata.FOGStats[i], jumpCount));
		
		Format(consoleOutput, sizeof consoleOutput,
			"%s tick %i: %7i | %5.2f%%%%\n",
			consoleOutput, i + 1,
			perfdata.FOGStats[i],
			CalcIntPercentage(perfdata.FOGStats[i], jumpCount));
	}
	
	PrintToChat(client, chatOutput);
	PrintToConsole(client, consoleOutput);
	
	return Plugin_Handled;
}

public Action Command_SmPerfstreaks(int client, int args)
{
	if (!IsValidClientExt(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iClientPerfstatsIndex[client] == -1)
	{
		CreatePerfstatsIndex(client);
	}
	
	// get custom steam id
	int customSteamID = 0;
	char szSteamID[32];
	if (args > 0)
	{
		GetCmdArg(1, szSteamID, sizeof szSteamID);
		
		char error[192];
		error = GetSteamIDFromString(szSteamID, sizeof szSteamID, customSteamID);
		if (error[0] != '\0')
		{
			PrintToChat(client, error);
			return Plugin_Handled;
		}
	}
	
	Perfstats perfdata;
	if (customSteamID == 0)
	{
		g_alPerfstatsFile.GetArray(g_iClientPerfstatsIndex[client], perfdata);
	}
	else
	{
		int index = GetPerfstatsIndexFromSteamID(customSteamID);
		if (index >= 0)
		{
			g_alPerfstatsFile.GetArray(index, perfdata);
		}
		else
		{
			PrintToChat(client, "%s SteamID \"%s\" was not found.", CHAT_PREFIX, szSteamID);
			return Plugin_Handled;
		}
	}
	
	char chatOutput[512];
	char consoleOutput[1024];
	
	int perfCount;
	for (int i; i < sizeof(perfdata.perfStreaks); i++)
	{
		// need to add up all the ones that come after the index,
		// because they are not counted for the current perf streaks.
		for (int j = i; j < sizeof(perfdata.perfStreaks); j++)
		{
			perfCount += perfdata.perfStreaks[j];
		}
	}
	
	Format(chatOutput, sizeof chatOutput, "%s ", CHAT_PREFIX);
	Format(consoleOutput, sizeof consoleOutput, "%s Perf streaks:\nPerf count: %i\n", PREFIX, perfCount);
	
	for (int i; i < sizeof(perfdata.perfStreaks); i++)
	{
		char percentageString[32];
		float percentage = CalcIntPercentage(perfdata.perfStreaks[i], perfCount);
		FormatEx(percentageString, sizeof percentageString, "%.2f", percentage);
		
		RemoveTrailing0s(percentageString);
		
		if (percentageString[0] == '0'
			&& percentageString[1] == '.')
		{
			// I can't find a function to copy a portion of a string, so let's do it inline!
			char buffer[sizeof percentageString];
			for (int strIndex; strIndex < sizeof(percentageString) - 1; strIndex++)
			{
				buffer[strIndex] = percentageString[strIndex + 1];
				if (buffer[strIndex] == '\0')
				{
					break;
				}
			}
			percentageString = buffer;
		}
		
		Format(chatOutput, sizeof chatOutput,
			"%s %i: "...C_LIME..."%s%%%%"...C_DEFAULT,
			chatOutput,
			i + 1,
			percentageString);
		
		Format(consoleOutput, sizeof consoleOutput,
			"%sPerfs %2i: %7i | %5.2f%%%%\n",
			consoleOutput, i + 1,
			perfdata.perfStreaks[i],
			percentage);
	}
	
	PrintToChat(client, chatOutput);
	PrintToConsole(client, consoleOutput);
	
	return Plugin_Handled;
}

public Action Command_SmScrollstats(int client, int args)
{
	if (!IsValidClientExt(client))
	{
		return Plugin_Handled;
	}
	
	if (g_iClientPerfstatsIndex[client] == -1)
	{
		CreatePerfstatsIndex(client);
	}
	
	// get custom steam id
	int customSteamID = 0;
	char szSteamID[32];
	if (args > 0)
	{
		GetCmdArg(1, szSteamID, sizeof szSteamID);
		
		char error[192];
		error = GetSteamIDFromString(szSteamID, sizeof szSteamID, customSteamID);
		if (error[0] != '\0')
		{
			PrintToChat(client, error);
			return Plugin_Handled;
		}
	}
	
	Perfstats perfdata;
	if (customSteamID == 0)
	{
		g_alPerfstatsFile.GetArray(g_iClientPerfstatsIndex[client], perfdata);
	}
	else
	{
		int index = GetPerfstatsIndexFromSteamID(customSteamID);
		if (index >= 0)
		{
			g_alPerfstatsFile.GetArray(index, perfdata);
		}
		else
		{
			PrintToChat(client, "%s SteamID \"%s\" was not found.", CHAT_PREFIX, szSteamID);
			return Plugin_Handled;
		}
	}
	
	char chatOutput[512];
	char consoleOutput[1024];
	
	int scrollCount;
	for (int i; i < sizeof(perfdata.scrollStats); i++)
	{
		scrollCount += perfdata.scrollStats[i];
	}
	
	Format(chatOutput, sizeof chatOutput, "%s Look in your console for scrollstats!", CHAT_PREFIX);
	Format(consoleOutput, sizeof consoleOutput, "%s Scroll stats:\nScroll count: %i\n", PREFIX, scrollCount);
	
	for (int i; i < sizeof(perfdata.scrollStats); i++)
	{
		int tick = SCROLL_GROUND_TICK - 1;
		if (i < tick)
		{
			Format(consoleOutput, sizeof consoleOutput, "%s     %3i: %7i | %5.2f%%%%\n", consoleOutput, i - tick, perfdata.scrollStats[i],
				CalcIntPercentage(perfdata.scrollStats[i], scrollCount));
		}
		else if (i == tick)
		{
			Format(consoleOutput, sizeof consoleOutput, "%sGROUND%2i: %7i | %5.2f%%%%\n", consoleOutput, i - tick, perfdata.scrollStats[i],
				CalcIntPercentage(perfdata.scrollStats[i], scrollCount));
		}
		else
		{
			Format(consoleOutput, sizeof consoleOutput, "%s      %2i: %7i | %5.2f%%%%\n", consoleOutput, i - tick, perfdata.scrollStats[i],
				CalcIntPercentage(perfdata.scrollStats[i], scrollCount));
		}
	}
	
	PrintToChat(client, chatOutput);
	PrintToConsole(client, consoleOutput);
	
	return Plugin_Handled;
}


public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (!IsValidClientExt(client, true)
		|| IsFakeClient(client))
	{
		return Plugin_Continue;
	}
	
	if (g_iClientPerfstatsIndex[client] == -1)
	{
		CreatePerfstatsIndex(client);
	}
	
	if (GetEntityFlags(client) & FL_ONGROUND)
	{
		g_movevars[client].framesOnGround++;
	}
	else
	{
		g_movevars[client].framesOnGround = 0;
	}
	
	bool jumped = (~g_movevars[client].lastButtons & IN_JUMP) && (buttons & IN_JUMP);
	
	Perfstats perfdata;
	g_alPerfstatsFile.GetArray(g_iClientPerfstatsIndex[client], perfdata);
	
	if (jumped)
	{
		if (g_movevars[client].framesOnGround)
		{
			g_movevars[client].hasJumped = true;
			if (g_movevars[client].framesOnGround == 1)
			{
				g_movevars[client].currentPerfStreak++;
			}
			else
			{
				// save perf streak
				if (g_movevars[client].currentPerfStreak > 0
					&& g_movevars[client].currentPerfStreak < MAX_PERF_STREAKS)
				{
					perfdata.perfStreaks[g_movevars[client].currentPerfStreak - 1]++;
				}
				g_movevars[client].currentPerfStreak = 0;
			}
			// save FOG stats
			if (g_movevars[client].framesOnGround < MAX_FRAMES_GROUND)
			{
				perfdata.FOGStats[g_movevars[client].framesOnGround - 1]++;
			}
		}
	}
	else if (g_movevars[client].framesOnGround == 1)
	{
		g_movevars[client].hasJumped = false;
	}
	
	if (g_movevars[client].framesOnGround == 1)
	{
		g_movevars[client].landingTick = GetGameTickCount();
	}
	
	// save currentScrolls (intentionally saved before reading current tick's scrolls
	if (g_movevars[client].hasJumped
		&& (GetGameTickCount() - g_movevars[client].landingTick) == SCROLL_GROUND_TICK)
	{
		for (int i; i < MAX_SCROLL_TICKS; i++)
		{
			if (g_movevars[client].currentScrolls & 1)
			{
				perfdata.scrollStats[i]++;
			}
			g_movevars[client].currentScrolls >>>= 1;
		}
		
		g_movevars[client].currentScrolls = 0;
	}
	
	g_movevars[client].currentScrolls <<= 1;
	if (jumped)
	{
		g_movevars[client].currentScrolls |= 1;
	}
	
	g_alPerfstatsFile.SetArray(g_iClientPerfstatsIndex[client], perfdata);
	
	g_movevars[client].lastButtons = buttons;
	
	return Plugin_Continue;
}

char[] GetSteamIDFromString(char[] szSteamID, int maxlen, int &result)
{
	char error[192];
	if (maxlen < 11)
	{
		FormatEx(error, sizeof error, "%s \"%s\" is too short for a steam id. Make sure you enclosed it in quotes: \"STEAM_X:X:XXXXX\"", CHAT_PREFIX, szSteamID);
		return error;
	}
	// check if this v is set, and add it to the result.
	//      STEAM_1:>0<:102468802
	if (szSteamID[8] == '1')
	{
		result += 1;
	}
	// not 0 or 1
	else if (szSteamID[8] != '0')
	{
		FormatEx(error, sizeof error, "%s \"%s\" isn't a valid SteamID. Make sure you enclosed it in quotes: \"STEAM_X:X:XXXXX\"", CHAT_PREFIX, szSteamID);
		return error;
	}
	
	// make STEAM_0 into STEAM_1
	if (szSteamID[6] == '0')
	{
		if (!ReplaceString(szSteamID, maxlen, "STEAM_0", "STEAM_1", false))
		{
			FormatEx(error, sizeof error, "%s \"%s\" isn't a valid SteamID. Only SteamID's in the format \"STEAM_X:X:XXXXX\" are supported.", CHAT_PREFIX, szSteamID);
			return error;
		}
	}
	// if it isn't 0 or 1
	else if (szSteamID[6] != '1')
	{
		FormatEx(error, sizeof error, "%s \"%s\" isn't a valid SteamID. Only SteamID's in the format \"STEAM_X:X:XXXXX\" are supported.", CHAT_PREFIX, szSteamID);
		return error;
	}
	// replace everything up to the last number with spaces
	//      STEAM_1:0:>102468802< (keep this)
	char lastSteamIDPart[32];
	strcopy(lastSteamIDPart, sizeof lastSteamIDPart, szSteamID);
	int len = strlen(lastSteamIDPart);
	for (int i; i < len && i < 10; i++)
	{
		lastSteamIDPart[i] = ' ';
	}
	
	int temp = StringToInt(lastSteamIDPart);
	if (temp == 0)
	{
		FormatEx(error, sizeof error, "%s \"%s\" isn't a valid SteamID. Expected a number higher than 0: STEAM_X:X:<this has to be higher than 0>", CHAT_PREFIX, szSteamID);
		return error;
	}
	
	result += temp * 2;
	
	if (result < 0)
	{
		result = 0;
		FormatEx(error, sizeof error, "%s \"%s\" isn't a valid SteamID. Negative number supplied or error reading integer.", CHAT_PREFIX, szSteamID);
		return error;
	}
	
	return error;
}

// @return    -1 if not found
int GetPerfstatsIndexFromSteamID(int steamID)
{
	int index = -1;
	for (int entry; entry < g_alPerfstatsFile.Length; entry++)
	{
		int storedSteamID = g_alPerfstatsFile.Get(entry, 0);
		
		if (steamID == storedSteamID)
		{
			index = entry;
			break;
		}
	}
	
	return index;
}

void OpenPerfstatsFile(char[] path)
{
	if (!FileExists(path))
	{
		return;
	}
	
	int fileSize = FileSize(path);
	File file = OpenFile(path, "rb");
	
	if (fileSize <= 0)
	{
		return;
	}
	
	g_alPerfstatsFile.Clear();
	for (int entry; entry < fileSize / sizeof(Perfstats) / 4; entry++)
	{
		any perfdata[sizeof Perfstats];
		file.Read(perfdata, sizeof(perfdata), 4);
		
		g_alPerfstatsFile.PushArray(perfdata);
	}
	
	file.Close();
	
}

void SavePerfstatsFile(char[] path)
{
	File file = OpenFile(path, "wb");
	
	for (int entry; entry < g_alPerfstatsFile.Length; entry++)
	{
		any perfdata[sizeof Perfstats];
		g_alPerfstatsFile.GetArray(entry, perfdata);
		
		file.Write(perfdata, sizeof(perfdata), 4);
	}
	
	file.Close();
}
